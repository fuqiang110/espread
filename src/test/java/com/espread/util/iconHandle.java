package com.espread.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.espread.common.utils.FileUtils;

/**
 * Icon Css 文件批处理
 * @author itdragons
 *
 */
public class iconHandle {

	public static void main(String[] args) {
		String iconStart =  "<div class=\"tree-icon";
		String iconEnd =  "\"></div>";
		String textStart = "<div class=\"tree-text\">";
		String textEnd = "</div>";
		
		String iconContent = FileUtils.readFile("d://icon-berlin.css");
		StringBuilder newContent = new StringBuilder();
		Pattern p=Pattern.compile("\\.icon(-\\w+)+"); 
		Matcher m=p.matcher(iconContent);
		while(m.find()){
			String icon = m.group().replace(".", "&nbsp;");
			newContent.append(iconStart).append(icon).append(iconEnd).append("\r\n");
			newContent.append(textStart).append(icon).append(textEnd).append("\r\n");
		}
		FileUtils.writeFile(newContent.toString(),"d://save_new.text");
	}
}
