package com.espread.sys.mapper;

import com.espread.sys.model.SysOrg;
import com.espread.sys.model.result.SysOrgVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysOrgMapper extends Mapper<SysOrg> {

    /**
     * 查询所有OrgVo
     *
     * @return
     */
    List<SysOrgVo> findAllOrgVo();

    /**
     * 查询所有部门
     *
     * @return
     */
    List<SysOrg> findAllOrg();

}