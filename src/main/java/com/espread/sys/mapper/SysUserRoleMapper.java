package com.espread.sys.mapper;

import com.espread.sys.model.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserRoleMapper {

    /**
     * 通过UserId查询角色Id集合
     *
     * @param userId
     * @return
     */
    List<String> findRoleIdListByUserId(String userId);

    /**
     * 通过UserId删除
     *
     * @param userId
     * @return
     */
    int deleteByUserId(String userId);

    int deleteByPrimaryKey(@Param("userId") String userId, @Param("roleId") String roleId);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);
}