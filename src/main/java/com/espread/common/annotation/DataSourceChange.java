package com.espread.common.annotation;

import java.lang.annotation.*;

/**
 * 数据源切换注解
 * 方法调用时添加数据源标识注解：@DataSourceChange(Const.DATASOURCE_MASTER)
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSourceChange {
    String value();
}
